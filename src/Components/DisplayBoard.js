import React from "react";

const DisplayBoard = ({ src }) => {
  return (
    <div className="display-board">
      <iframe className="display-board__iframe" src={src} />
    </div>
  );
};

DisplayBoard.defaultProps = {
  src: "http://www.leg.state.co.us/public/display.nsf/lpusersen.html"
};

export default DisplayBoard;
