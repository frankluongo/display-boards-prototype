import React from "react";
import Tabs from "./Components/Tabs";
import DisplayBoard from "./Components/DisplayBoard";

const Application = () => {
  return (
    <main>
      <div className="container m-xsmall-top-1 m-xsmall-bottom-1 m-medium-top-3 m-medium-bottom-3">
        <Tabs
          tabIdentifier="display-boards"
          tabs={[
            {
              tabLabel: "House Floor",
              tabContent: (
                <DisplayBoard src="http://www.leg.state.co.us/public/display.nsf/hlpuser.html" />
              )
            },
            {
              tabLabel: "Senate Floor",
              tabContent: (
                <DisplayBoard src="http://www.leg.state.co.us/public/display.nsf/slpuser.html" />
              )
            },
            {
              tabLabel: "House Committees",
              tabContent: (
                <DisplayBoard src="http://www.leg.state.co.us/public/display.nsf/hccuser.html" />
              )
            },
            {
              tabLabel: "Senate Committees",
              tabContent: (
                <DisplayBoard src="http://www.leg.state.co.us/public/display.nsf/sccuser.html" />
              )
            }
          ]}
        />
      </div>
    </main>
  );
};

export default Application;
